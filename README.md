## :books: Daftar Repository Tugas Binar Academy (:sunny: _Kelas Backend Sesi Siang_)

### Agung

- Github

  - Day 3 & 4 Week 1 - [Day-3-4-NodeJS-Basic-Github](https://github.com/triagungtio07/Day-3-4-NodeJS-Basic-Github)
  - Day 1-3 Week 2- [Week-2---Day-1-3-Javasript-Basic-](https://github.com/triagungtio07/Week-2---Day-1-3-Javasript-Basic-)
  - Day 1-2 Week 3- [Week-3---Day-1-2-Star_Looping ](https://github.com/triagungtio07/Week-3---Day-1-2-Star_Looping)

- Gitlab

  - Day 3 & 4 Week 1- [Day 3 and 4 NodeJS Basic Gitlab ](https://gitlab.com/triagungtio07/day-3-and-4-nodejs-basic-gitlab)
  - Day 1-3 Week 2- [Week 2 Day 1-3](https://gitlab.com/triagungtio07/week-2-day-1-3)
  - Day 1-2 Week 3- [Week 3 Day 1-2](https://gitlab.com/triagungtio07/week-3-day-1-2)
  - Week 4 [Week 4 Binar Academy](https://gitlab.com/triagungtio07/week-4-binar-academy)
  - Week 5 [Week 5 Binar Academy](https://gitlab.com/triagungtio07/week-5-binar-academy)
  - Week 6 [Week 6 Binar Academy](https://gitlab.com/triagungtio07/week-6-binar-academy)
  - Week 7 [Week 7 Binar Academy](https://gitlab.com/triagungtio07/week-7-binar-academy)
  - Week 8 [Week 8 Binar Academy](https://gitlab.com/triagungtio07/week-8-binar-academy)

- Catatan
  - https://www.notion.so/Notes-Materi-Backend-400fecb1fa284675a745db25de9a791b

### Andrea

- GitLab
  - Week 2 - 7 - [Binar Academy](https://github.com/GOndronggg/BinarAcademy.git)
- Catatan
  - [Pin-Bar](https://www.notion.so/Pin-Bar-17c372134fb841c587a5f02bcda4317f)

### Arga

- Github
  - Week 1 - [Week-1](https://github.com/anaskaciw/Week-1)
  - Week 2 - [Week-2](https://github.com/anaskaciw/Week-2)
- Gitlab
  - Week 2 - [Week-2](https://gitlab.com/Anaskaciw/day-3-and-4-basic-nodejs)
- Catatan
  - URL_FILE_CATATAN
    [Notion](https://www.notion.so/Bootcamp-Backend-Batch-10-9fe2790f62cf4d0b86194c53945f4e0d)

### Bayu

- Github

  - Week 1_Day 3 & 4 - [Week-1_day-3-4](https://github.com/bayuubay/Week-1_day-3-4)
  - Week 2_Day 1 & 2 & 3- [week-2-day-1-2](https://github.com/bayuubay/week-2-day-1-2)
  - Week 3_Day 1 & 2 - [Week3_day-1-2](https://github.com/bayuubay/Week3_day-1-2)
  - Week 4_Day 1 - [Week4](https://github.com/bayuubay/week4day1)
  - Week 4_Day 1 - [Week4](https://github.com/bayuubay/week4day2)
  - Week 4_Day 1 - [Week4](https://github.com/bayuubay/week4day3)
  - Week 5_Day 1 - [Week5](https://github.com/bayuubay/week5-day1)
  - Week 5_Day 3 - [Week5](https://github.com/bayuubay/week5-day3)
  - Week 5_Day 4 - [Week5](https://github.com/bayuubay/week5-day4)
  - Week 5_Day 5 - [Week5](https://github.com/bayuubay/week5-day5)
  - Week 5_Day 6 - [Week4](https://github.com/bayuubay/week5-day6-codeChallenge)
  - Week 6_Day 3 - [Week5](https://github.com/bayuubay/week6-day3-middleware)
  - Week 7_Day 2 - [Week7](https://github.com/bayuubay/week7-day2-mongoose)
  - Week 8_final-project- [Week8](https://github.com/bayuubay/week8)
  - Week 9_final-project- [Week9](https://github.com/bayuubay/week9-finalProject)

- Gitlab
  - Day 3 & 4 - [week-1_day-3-4](https://gitlab.com/bayuubay/week-1_day-3-4)
- Catatan
  - https://www.notion.so/Notes-Backend-daa351f72a87459eac4ad3cf7bf46b91
  - https://www.notion.so/GA-bootcamp-10-BE-class-daily-notes-7e8745e6553b42bdb97a17a7563b8653

### Eka Diah Cahyani

- Day 1 & 2 - [Day-1-dan-2-Javascript-Basic-Github](https://github.com/ekadyah/Day-1-dan-2-Javascript-Basic-Github)
- Day 3 & 4 - [Day-1-dan-2-Javascript-Basic-Github](https://github.com/ekadyah/Day-1-dan-2-Javascript-Basic-Github)

- Gitlab
  - Week1 - https://gitlab.com/ekadyah/binar-academy-x-glints-academy/-/tree/master/Week1
  - Week2 - https://gitlab.com/ekadyah/binar-academy-x-glints-academy/-/tree/master/Week2
  - Week3 - https://gitlab.com/ekadyah/binar-academy-x-glints-academy/-/tree/master/Week3
  - Week4 - https://gitlab.com/ekadyah/binar-academy-x-glints-academy/-/tree/master/Week4
  - Week5 - https://gitlab.com/ekadyah/binar-academy-x-glints-academy/-/tree/master/Week5
  - Week6 - https://gitlab.com/ekadyah/binar-academy-x-glints-academy/-/tree/master/Week6
  - Week7 - https://gitlab.com/ekadyah/binar-academy-x-glints-academy/-/tree/master/Week7
  - Week8 - https://gitlab.com/ekadyah/binar-academy-x-glints-academy/-/tree/master/Week8
- Catatan - https://www.notion.so/GA-BATCH-1O-d60171c47029405989f59ea9293f73d4

### Kurniawan Agni

- Github

  - Day 3 & 4 Week 1- Day 3 and 4 NodeJS Basic Gitlab
  - Day 1-3 Week 2- Week 2 Day 1-3
  - Day 1-2 Week 3- Week 3 Day 1-2
  - [Week 4](https://github.com/awandilangitt/week_4)
  - [Week 5](https://github.com/awandilangitt/week_5)
  - [Week 6](https://github.com/awandilangitt/week_6)
  - [Week 7](https://github.com/awandilangitt/week_7)
  - [Week 8](https://github.com/awandilangitt/week_8)

- Catatan
  [Notion](https://www.notion.so/Me-Notes-0373689c03a44f388f5e86338af264ba)

### Muhammad Laidri Fernanda

- Github
  - Day 3 & 4 - [Nama repository](https://gitlab.com/-/ide/project/softwaremakassar/binar-academy-students-batch-10/)
- Gitlab
  - Week 1 - [Week 1](https://gitlab.com/laidrifernanda/Week-1)
  - Week 2 - [Week 2](https://gitlab.com/laidrifernanda/Week-2)
  - Week 3 - [Week 3](https://gitlab.com/laidrifernanda/Week-3)
  - Week 4 - [Week 4](https://gitlab.com/laidrifernanda/Week-4)
  - Week 5 - [Week 5](https://gitlab.com/laidrifernanda/Week-5)
  - Week 6 - [Week 6](https://gitlab.com/laidrifernanda/Week-6)
  - Week 7 - [Week 7](https://gitlab.com/laidrifernanda/Week-7)
  - Week 8 - [Week 8](https://gitlab.com/laidrifernanda/Week-8)
- Catatan
  - https://www.notion.so/Catatan-Fernanda-759f7a96220c42caae36fcdc271479fe

### Tamam

- Github
  - Day 3 & 4 - [Day-3-dan-4-NodeJS---Basic-Github](https://github.com/Tamam-Ahda/Day-3-dan-4-NodeJS---Basic-Github.git)
- Gitlab
  - Week 1: https://gitlab.com/tamam_ahda/materi-binar-academy/-/tree/master/Week%201
  - Week 2: https://gitlab.com/tamam_ahda/materi-binar-academy/-/tree/master/Week%202
  - Week 3: https://gitlab.com/tamam_ahda/materi-binar-academy/-/tree/master/Week%203
  - Week 4: https://gitlab.com/tamam_ahda/materi-binar-academy/-/tree/master/Week%204
  - Week 5: https://gitlab.com/tamam_ahda/materi-binar-academy/-/tree/master/Week%205
  - Week 6: https://gitlab.com/tamam_ahda/materi-binar-academy/-/tree/master/Week%206
  - Week 7: https://gitlab.com/tamam_ahda/materi-binar-academy/-/tree/master/Week%207
  - Week 8: https://gitlab.com/tamam_ahda/materi-binar-academy/-/tree/master/Week%208
- Catatan
  - https://www.notion.so/Notes-Materi-Backend-eef737a02dfe4bf1924cafc5a0dd089a
